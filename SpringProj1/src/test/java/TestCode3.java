import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.jpmc.AppConfiguration;
import com.jpmc.component.CustomerService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=AppConfiguration.class)
@ActiveProfiles("dev")
@Sql(value="schema.sql")
//@Transactional
public class TestCode3 {
	
	@Autowired
	private CustomerService cs;
	
	@Test
	@DirtiesContext
	public void testCode() {
//		ApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfigration.class);
//		CustomerService cs = ctx.getBean(CustomerService.class);
		assertNotNull(cs);
		cs.call();
	}
}
