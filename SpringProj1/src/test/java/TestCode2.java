import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.jpmc.AppConfiguration;
import com.jpmc.component.CustomerService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=AppConfiguration.class)
@ActiveProfiles("dev")
public class TestCode2 {
	
	@Autowired
	private CustomerService cs;
	
	@Test
	public void testCode() {
//		ApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfigration.class);
//		CustomerService cs = ctx.getBean(CustomerService.class);
		assertNotNull(cs);
	}
}
