import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.jpmc.AppConfiguration;
import com.jpmc.component.CustomerService;

public class TestCode {
	@Test
	public void testCode() {
		ApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfiguration.class);
		CustomerService cs = ctx.getBean(CustomerService.class);
		assertNotNull(cs);
	}
}
