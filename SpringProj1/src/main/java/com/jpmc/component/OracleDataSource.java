package com.jpmc.component;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("prod")
public class OracleDataSource implements DataSource  {

}
