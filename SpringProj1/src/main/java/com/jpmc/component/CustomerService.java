package com.jpmc.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

//@Component
//@Lazy
//@Scope("prototype")
@Service
public class CustomerService {
	
	@Autowired
	private CustomerDao customerDao;
	
	public void call() {
		customerDao.call();
	}
	

}
