package com.jpmc.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

//@Repository
public class CustomerDao {
	
	@Autowired
	private DataSource dataSource;
	
	public void call() {
		System.out.println(dataSource);
	}
}
