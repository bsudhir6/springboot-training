package com.jpmc;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.jpmc.component.CustomerDao;

@Configuration
@ComponentScan(basePackages="com.jpmc.component")
public class AppConfiguration {

	@Bean
	public CustomerDao customerDao() {
		return new CustomerDao();
	}
}
