package com.jpmc.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.jpmc.Application;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=Application.class)
//@SpringBootTest(classes=HelloController.class)
@AutoConfigureMockMvc
public class TestCode {
	
	@Autowired
	private MockMvc mvc;
	
	@Test
	public void testHelloController() throws Exception {
		mvc.perform(MockMvcRequestBuilders
				.get("/hello"))
				.andExpect(MockMvcResultMatchers.content().string("Welcome to Spring Boot"));
	}
	
}
