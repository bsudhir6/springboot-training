package com.jpmc.test;

import static org.junit.Assert.assertNotNull;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.jpmc.Application;
import com.jpmc.entity.Trade;
import com.jpmc.repository.TradeRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
@SpringBootTest(classes=Application.class)
public class TradeRepositoryTest {
	@Autowired
	private TestEntityManager entityManager;
	
	@Autowired
	private TradeRepository tradeRepository;
	
	@Test
	public void testScenario() {
		Trade trade = new Trade();
		trade.setTradeId(23);
		trade.setAmount(45000);
		trade.setRegion("NY");
		trade.setDate(new Date());
		
		entityManager.persist(trade);
		
		Trade fetchedTrade = tradeRepository.findById(23).get();
		assertNotNull(fetchedTrade);
	}
}
