package com.jpmc.client;

import java.util.Date;

import org.springframework.web.client.RestTemplate;

import com.jpmc.entity.Trade;

public class TradeClient {

	public static void main(String[] args) {
		RestTemplate restTemplate = new RestTemplate();
		
		Trade trade = new Trade();
		trade.setTradeId(11111);
		trade.setAmount(45000);
		trade.setRegion("NY");
		trade.setDate(new Date());
		
		String response = restTemplate.postForObject("http://localhost:8080/trade/new", trade, String.class);
		System.out.println(response);
	}
}
