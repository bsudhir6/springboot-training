package com.jpmc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.jpmc.entity.Trade;
import com.jpmc.repository.TradeRepository;

@Service
public class TradeService {
	@Autowired
	private TradeRepository tradeRepository;
	
	@CacheEvict(value="trades.cache")
	public void newTrade(Trade trade) {
		tradeRepository.save(trade);
	}
	
	@Cacheable("trades.cache")
	public Iterable<Trade> fetchAllTrades(){
		return tradeRepository.findAll();
	}
}
