package com.jpmc.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.jpmc.entity.Trade;

@Repository
public interface TradeRepository extends CrudRepository<Trade, Integer> {

}
