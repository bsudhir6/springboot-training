package com.jpmc.controller;

import javax.annotation.security.RolesAllowed;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jpmc.entity.Trade;
import com.jpmc.service.TradeService;

@RestController
@RequestMapping(path="/trade/*")
public class TradeController {

	@Autowired
	private TradeService tradeService;
	
	@GetMapping(path="/all", produces= {"application/json", "application/xml"})
	@RolesAllowed("ADMIN")
	public Iterable<Trade> all(){
		return tradeService.fetchAllTrades();
	}
	
	@PostMapping(path="/new", consumes = {"application/json", "application/xml"})
	public String add(@RequestBody Trade trade) {
		tradeService.newTrade(trade);
		return "Trade record created successfully";
	}
}
