package com.jpmc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication // equivalent of using @Component, @EnableAutoConfiguration, @ComponentScan
//@EnableAutoConfiguration(exclude=DataSourceAutoConfiguration.class)
@EnableJpaRepositories(basePackages = "com.jpmc.repository")
@EntityScan(basePackages = "com.jpmc.entity")
@EnableCaching
public class Application {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SpringApplication.run(Application.class, args);

	}

}
